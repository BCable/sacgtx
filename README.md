Super Advanced Calculator XXL GTX Turbo
=======================================

Shiny application and Slidify combo project for Developing Data Products from Johns Hopkins University.

This is what happens when you are working so much that you are sleep deprived and realize that you have a project due in two days, yet will be installing multiple servers, installing operating systems, doing massive system migrations, etc during that time and decide to do all of the project in the night before you are going to perform those migrations.

Do not do this at home, kids.

[Slidify Presentation](https://bcable.gitlab.io/sacgtx)

[Shiny Application](https://bcable.shinyapps.io/sacgtx)
